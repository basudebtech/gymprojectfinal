/**
 * hamburger.js
 *
 * Mobile Menu Hamburger
 * =====================
 * A hamburger menu for mobile websites & apps
 *
 * Created by Debanjan Basu  |
 */

jQuery(document).ready(function () {

    //Open the menu
    jQuery("#hamburger").click(function () {

        

        jQuery('nav').animate({left: '0%'});

        //set the width of primary content container -> content should not scale while animating
       

        //display a layer to disable clicking and scrolling on the content while menu is shown
        jQuery('#contentLayer').css('display', 'block');

        //disable all scrolling on mobile devices while menu is shown
        jQuery('#container').bind('touchmove', function (e) {
            e.preventDefault()
        });

        //set margin for the whole container with a jquery UI animation
        

    });

    //close the menu
    jQuery("#contentLayer").click(function () {

        //enable all scrolling on mobile devices when menu is closed
        jQuery('#container').unbind('touchmove');

       
                jQuery('nav').animate({left: '-70%'});
               

            
        });
		jQuery('nav').click(function () {

        //enable all scrolling on mobile devices when menu is closed
        jQuery('#container').unbind('touchmove');

       
        jQuery('nav').animate({left: '-70%'});
    });

});